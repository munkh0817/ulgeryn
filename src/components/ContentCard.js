import * as React from 'react';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import FavoriteIcon from '@mui/icons-material/Favorite';
import IconButton from '@mui/material/IconButton';
import { pink } from '@mui/material/colors';
import { useNavigate } from 'react-router-dom';
import { Link } from "react-router-dom";

function ContentCard(props) {
  const { post } = props;

  // const navigate = useNavigate();

  // const navigateToContent = () => {
  //   // console.log(props.title);
  //   navigate('/Content')
  // };

  return (
    <Grid item xs={12} md={6} >
      <CardActionArea component="a" href="#">
        <Card sx={{ display: 'flex' }} minHeight="300px">
          <Grid container spacing={0} columns={{ xs: 4, md: 12 }}  alignItems="center"  justifyContent="center" direction="row">
            <Grid item xs={6}>
              <CardMedia
                component="img"
                sx={{ width: 250, height:150, display: { xs: 'none', sm: 'block' }, pt: 2, pl: 2 }}
                image={post.imageUrl}
                // alt={post.imageLabel}
              />
            </Grid>
            <Grid item xs={6} >
              <CardContent sx={{ flex: 1 , p: 0}} >    
                <Grid container spacing={0} columns={{ xs: 4, md: 12 }} >   
                  <Grid item xs={12} sx={{ pt: 5}}>
                    <Typography component="h2" variant="h6">
                    {post.title}
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary">
                      {post.categoryId}
                    </Typography>
                    <Typography variant="subtitle2" color="text.secondary">
                      {post.authorId}
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography variant="subtitle2" color="text.secondary">
                      {post.publisheDdate}
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                      <IconButton aria-label="add to favorites" >
                        <FavoriteIcon  sx={{ fontSize: 18 , color: pink[500]}}/>
                      </IconButton>        
                      <span style={{color: pink[500]}}>                  
                        {post.likeNumber}                     
                      </span>
                  </Grid>
                </Grid>  
              </CardContent>  
            </Grid>
            <Grid item xs={12}>
              <CardContent sx={{ flex: 1 , pt:0}}>
                {/* <Typography variant="subtitle1" color="primary">
                    {post.tagWords.map((tagWord) => (
                      <span>#{tagWord}</span>
                  ))}
                </Typography> */}
                <Typography variant="subtitle1" paragraph>
                  {post.text}
                </Typography>
                <Link to={`/Content/${post.id}`} >
                <Typography variant="subtitle1" color="primary" 
                // onClick={navigateToContent} 
                >
                  Үргэлжлүүлэх...
                </Typography>
                </Link>
              </CardContent> 
            </Grid>
           
          </Grid>   
        </Card>
      </CardActionArea>
    </Grid>
  );
}

ContentCard.propTypes = {
  post: PropTypes.shape({
    date: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    imageLabel: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }).isRequired,
};

export default ContentCard;