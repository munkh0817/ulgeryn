import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Link } from "react-router-dom";
import { createTheme, ThemeProvider } from '@mui/material/styles';
import MainFeaturedPost from './MainFeaturedPost';
import ContentCard from './ContentCard';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { useEffect } from 'react';
import {setContents} from "../redux/actions/contentActions"

function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Ulgeryn
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const mainFeaturedPost = {
  title: 'Үлгэрийн цахим сан',
  description:
    "Манай сайтаас олон сонирхолтой үлгэр олж унших боломжтой",
  image: 'https://img.freepik.com/free-vector/magic-fairytale-concept_23-2148463220.jpg?w=2000',
  imageText: 'main image description',
};


const theme = createTheme();

export default function Home() {

  const contents = useSelector((state) => state.allContents.contents)
  const dispatch = useDispatch();

  const fetchContents = async () => {
      const response = await axios
      .get("/api/v1/contents")
      .catch((err) => {
        console.log("Err", err)
      });
      dispatch(setContents(response.data));
  }
  useEffect(() => {
    fetchContents();
  }, []);

  
  //const { title, description} = contents[0]
  console.log("Contents; ", contents)
 
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container maxWidth="lg">
        <main>
          <MainFeaturedPost post={mainFeaturedPost} />
          <Grid container spacing={4}>
            {contents.map((post) => (
              
              <ContentCard key={post.id} post={post} />
            ))}
          </Grid>
        </main>
      </Container>
      {/*Footer*/}
      <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
        <Typography variant="h6" align="center" gutterBottom>
          Footer
        </Typography>
        <Typography
          variant="subtitle1"
          align="center"
          color="text.secondary"
          component="p"
        >
          Манай сайтыг ашиглаж буйд талархлаа!
        </Typography>
        <Copyright />
      </Box>
    </ThemeProvider>
  );
}