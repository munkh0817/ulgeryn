import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import styled, { keyframes } from 'styled-components';
import {bounce} from 'react-animations'
import { Image } from 'mui-image'
import FavoriteIcon from '@mui/icons-material/Favorite';
import IconButton from '@mui/material/IconButton';
import { pink } from '@mui/material/colors';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios'
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { removeSelectedContent, selectedContent } from '../redux/actions/contentActions';


const bounceAnimation = keyframes`${bounce}`;

const BouncyDiv = styled.div`
  animation: 1s ${bounceAnimation};
`;

const theme = createTheme({
    typography: {
      fontFamily: 'Cormorant Infant',
    }
  });

  const post = {
      title: 'Болдоггүй бор өвгөн',
      date: 'Nov 12',
      likeNumber : 99,
      author : 'Munkhamgalan',
      category: 'Монгол ардын',
      tagWords: ['болдоггүй', 'бор', 'өвгөн'],
      description:
        'This is a wider card with supporting text below as a natural lead-in to additional content.Multiple lines of text that form the lede, informing new readers quickly and efficiently about what',
      image: 'https://image.winudf.com/v2/image/Y29tLm1vbmRyb2lkLnV2Z3VuX3NjcmVlbnNob3RzXzVfNmEyMGNiOWI/screen-5.jpg?fakeurl=1&type=.webp',
      imageLabel: 'Image Text',
  }


export default function Content() { 

    const post = useSelector((state) => state.content)
    const { contentId } = useParams();
    const dispatch = useDispatch();
    console.log("contentId: ", contentId)

    const fetchContentDetail = async () => {
      const response = await axios
      .get(`/api/v1/content/${contentId}`)
      .catch((err) => {
        console.log("Err", err)
      });
      
      dispatch(selectedContent(response.data));
    }

    useEffect(() => 
      {
        if(contentId && contentId!=="") fetchContentDetail();
        return () => {
          dispatch(removeSelectedContent())
        }
      } , [contentId]);


    return (
    <ThemeProvider theme={theme}>
        <Container component="about" >
        <CssBaseline />
            <Box
                sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                }}
            >
                <BouncyDiv >
                    <Typography component="h2" variant="h2" >
                            Болдоггүй өвөө
                    </Typography>
                </BouncyDiv >
            </Box>
            <Box>
                <Grid container spacing={0} columns={{ xs: 4, md: 12 }}  alignItems="center"  justifyContent="center" direction="row">
                    <Grid item xs={6}>
                        <Image src=''/>
                    </Grid>
                    <Grid item xs={6}>
                    <Grid container spacing={0} columns={{ xs: 4, md: 12 }} >   
                  <Grid item xs={12} sx={{ pt: 5}}>
                    <Typography component="h2" variant="h6">
                    {post.title}
                    </Typography>
                    <Typography variant="subtitle1" color="text.secondary">
                      {post.categoryId}
                    </Typography>
                    <Typography variant="subtitle2" color="text.secondary">
                      {post.authorId}
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography variant="subtitle2" color="text.secondary">
                      {post.publishedDate}
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                      <IconButton aria-label="add to favorites" >
                        <FavoriteIcon  sx={{ fontSize: 18 , color: pink[500]}}/>
                      </IconButton>        
                      <span style={{color: pink[500]}}>                  
                        {post.likeNumber}                     
                      </span>
                  </Grid>
                </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    </ThemeProvider>
);
}