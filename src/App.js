import * as React from 'react';
import About from "./components/About"
import Content from "./components/Content"
import { BrowserRouter, Route, Routes} from "react-router-dom";
import LogIn from "./components/LogIn";
import SignUp from "./components/SignUp";
import Home from './components/Home';
import NavigationBar from './components/NavigationBar';


function App() {
  return (
    <div className="App">
      <BrowserRouter>  
      <NavigationBar/>   
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/About" element={<About/>} />
          <Route path="/LogIn" element={<LogIn/>} />
          <Route path="/SignUp" element={<SignUp/>} />
          {/* <Route path="/Content" element={<Content/>} /> */}
          <Route path="/Content/:contentId" element={<Content/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
