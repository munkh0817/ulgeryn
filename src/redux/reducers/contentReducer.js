import { Navigate } from "react-router";
import {ActionTypes} from  "../constants/action-types"

const initialState = {
    contents:[
        // {
        //     title: 'Болдоггүй бор өвгөн',
        //     date: 'Nov 12',
        //     likeNumber : 99,
        //     author : 'Munkhamgalan',
        //     category: 'Монгол ардын',
        //     tagWords: ['болдоггүй', 'бор', 'өвгөн'],
        //     description:
        //       'This is a wider card with supporting text below as a natural lead-in to additional content.Multiple lines of text that form the lede, informing new readers quickly and efficiently about what',
        //     image: 'https://image.winudf.com/v2/image/Y29tLm1vbmRyb2lkLnV2Z3VuX3NjcmVlbnNob3RzXzVfNmEyMGNiOWI/screen-5.jpg?fakeurl=1&type=.webp',
        //     imageLabel: 'Image Text',
        //   },
        
    ],
}

// how to store 

export const contentReducer = (state = initialState, {type, payload}) => {
    switch (type){
        case ActionTypes.SET_CONTENTS: 
            return {...state, contents: payload};
        // case ActionTypes.READ_CONTENT: 
            
        //     if(isAvailableByUserId){
        //         navigate('/read/${id}')
        //     } 
        // case ActionTypes.GET_TEXT: 
        //     ba
        default:
            return state;
    }}
export const selectedContentReducer = (state = {}, {type, payload}) => {
    switch (type){
        case ActionTypes.SELECTED_CONTENT: 
            return {...state, ...payload};
        case ActionTypes.REMOVE_SELECTED_CONTENT: 
            return {};
        default:
            return state;
    }}

