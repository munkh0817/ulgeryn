import { combineReducers } from "redux";
import { contentReducer, selectedContentReducer } from "./contentReducer";


const reducers = combineReducers({
    allContents: contentReducer,
    content: selectedContentReducer,
});

export default reducers;