import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Container from '@mui/material/Container';
import Tooltip from '@mui/material/Tooltip';
import BookIcon from '@mui/icons-material/Book';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Stack from '@mui/material/Stack';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Button from '@mui/material/Button';
import { Pages } from '@material-ui/icons';
import Grid from '@mui/material/Grid';

const pages = ['Нүүр хуудас','Тухай'];
const settings = ['Профайл', 'VIP хэрэглэгч болох', 'Гарах'];

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

function ResponsiveAppBar() {
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const [categorySelected, setCategorySelected] = React.useState(1);

  const categorySelectionChangeHandler = (event) => {
    setCategorySelected(event.target.value);
  };


  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar spacing={3} disableGutters>
        {/* <BookIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} /> */}
        <Grid container spacing={1} columns={{ xs: 4, md: 16 }}  alignItems="center"  justifyContent="center" direction="row">
          <Grid item xs={8} >
            <Grid container spacing={0} columns={{ xs: 4, md: 10 }}  alignItems="center"  justifyContent="flex-start" direction="row">
              <Grid item xs={3} >
                <Typography
                    variant="h6"
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      m: 1,
                      display: { xs: 'none', md: 'flex' },
                      fontFamily: 'monospace',
                      fontWeight: 700,
                      letterSpacing: '.3rem',
                      color: 'inherit',
                      textDecoration: 'none',
                    }}
                >
                    Ulgeryn
                </Typography>
              </Grid>

              <Grid item xs={2}>
                <Button sx={{ color: '#fff' ,textTransform: 'none'}}>
                    {pages[0]}
                </Button>
              </Grid>

              <Grid item xs={1.5}>
                <Button sx={{ color: '#fff', textTransform: 'none'}}>
                {pages[1]}
                </Button>
              </Grid>
              
              <Grid item xs={2}>
                <FormControl variant="standard"  sx={{ m: 0, minWidth: 120,
                    display: { xs: 'none', md: 'flex' }}}>
                  <Select value={categorySelected} onChange={categorySelectionChangeHandler} disableUnderline 
                  style={{color: 'white'}} >
                    <MenuItem value={1}>Монгол ардын үлгэр</MenuItem>
                    <MenuItem value={2}>Гадаад үлгэр</MenuItem>
                    <MenuItem value={3}>March</MenuItem>
                    <MenuItem value={4}>April</MenuItem>
                    <MenuItem value={5}>May</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={8} >
            <Grid container spacing={8} columns={{ xs: 4, md: 8 }}  alignItems="center"  justifyContent="flex-end" direction="row">
              <Grid  item xs={6}> 
                <Search>
                  <SearchIconWrapper>
                    <SearchIcon />
                  </SearchIconWrapper>
                  <StyledInputBase
                    placeholder="Search…"
                    inputProps={{ 'aria-label': 'search' }}
                  />
                </Search>
              </Grid>                  
              <Grid  item xs={1}> 
                <Box sx={{ flexGrow: 0 }}>
                  <Tooltip title="Open settings">
                    <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }} color="inherit">
                      {/* <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" /> */}
                      <AccountCircle />
                    </IconButton>
                  </Tooltip>
                  <Menu
                    sx={{ mt: '45px' }}
                    id="menu-appbar"
                    anchorEl={anchorElUser}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={Boolean(anchorElUser)}
                    onClose={handleCloseUserMenu}
                  >
                    {settings.map((setting) => (
                      <MenuItem key={setting} onClick={handleCloseUserMenu}>
                        <Typography textAlign="center">{setting}</Typography>
                      </MenuItem>
                    ))}
                  </Menu>
                </Box>
              </Grid> 
            </Grid>
          </Grid>     
          </Grid>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
export default ResponsiveAppBar;
